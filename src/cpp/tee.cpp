/**
 * @file tee.cpp
 * @author Joseph Abboud (joseph.abboud@epfl.ch)
 *
 * @brief This file contains a C++ code that creates and maintains a
 * table which stores the label distribution of each node of a distributed
 * machine learning network. There is a C interface that can be used by a
 * python script to call the C++ functions.
 *
 * @note To compile and run this code in python, do the following steps:
 * . From the terminal, go to the directory containing this file
 * . Compile the code with:           g++ -c -fPIC tee.cpp -o tee.o
 * . Turn it into a shared library:   g++ -shared -o libtee.so tee.o
 * . Run a python script that uses ctypes
 */

#include <iostream>
#include "similarity.cpp"
#include <vector>

using namespace std;

/**
 * @brief Struct representing a label
 *
 * @param label Label ID
 * @param count Number of datapoints for this label in the node
 *
 */
struct data
{
    size_t label;
    uint64_t count;
};

/**
 * @brief Struct representing a node.
 *
 * @param nodeId UID of the node in the system
 * @param numData Number of unique labels in the node's local data
 * @param labels Pointer to the first label in an array of labels, with size numData
 *
 */
struct node
{
    size_t nodeId;
    size_t numData;
    struct data* labels;
};

/**
 * @brief Struct representing a TEE.
 *
 * @param numNodes Number of nodes in the machine learning network
 * @param nodes Pointer to the first node in an array of nodes, with size numNodes
 *
 */
struct tee
{
    size_t numNodes;
    struct node* nodes;
};

vector<uint64_t> vectoriseNode(struct node node)
{
    vector<uint64_t> vec;

    for (size_t i = 0; i < node.numData; i++)
    {
        vec.push_back(node.labels[i].count);

        // Uncomment lines below for debugging purposes
        // cout << "Node " << to_string(node.nodeId) << endl;
        // cout << to_string(node.labels[i].label) << ": " << to_string(node.labels[i].count) << endl;
    }

    return vec;
}

extern "C" {

    /**
     * @brief Computes the similarity between the label distribution of two nodes
     *
     * @param tee The Trusted Execution Environment
     * @param node1 UID of the first node
     * @param node2 UID of the second node
     * @return double The computed similarity
     */
    double similarity(const struct tee& tee, size_t node1, size_t node2)
    {
        if (node1 >= tee.numNodes || node2 >= tee.numNodes)
        {
            cerr << "The given UIDs are out of bounds" << endl;
        }

        // Using cosine similarity here, you can use other metrics defined in similarity.cpp
        return cosineSimilarity(vectoriseNode(tee.nodes[node1]), vectoriseNode(tee.nodes[node2]));
    }

    /**
     * @brief Print the contents of a data struct
     *
     * @param data Reference to the data struct
     */
    void print_data(const struct data& data)
    {
        cout << data.label << ": " << data.count;
    }

    /**
     * @brief Print the contents of a node struct
     *
     * @param node Reference to the node struct
     */
    void print_node(const struct node& node)
    {
        cout << "Node " << node.nodeId << ": {";
        for (size_t i = 0; i < node.numData; i++)
        {
            print_data(node.labels[i]);
            cout << ", ";
        }
        cout << "}" << endl;
    }

    /**
     * @brief Print the contents of a tee struct
     *
     * @param tee Reference to the tee struct
     */
    void print_tee(const struct tee& tee)
    {
        for (size_t i = 0; i < tee.numNodes; i++)
        {
            print_node(node(tee.nodes[i]));
        }
    }
}
