# This script takes two args: $1 is the directory containing the scripts 
# and $2 is the name of the c++ file (no extension) that we want to turn into a library
cd $1
g++ -std=c++17 -c -fPIC $2.cpp -o $2.o && g++ -shared -o lib$2.so $2.o
