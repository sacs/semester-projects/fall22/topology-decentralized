/**
 * @file similarity.cpp
 * @author Joseph Abboud (joseph.abboud@epfl.ch)
 *
 * @brief File used to compute different similarity metrics for 2 vectors
 */

#include <numeric>
#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

bool areValid(vector<uint64_t> x, vector<uint64_t> y)
{
    if (x.size() != y.size())
    {
        cerr << "Given vectors have different sizes" << endl;
        return false;
    }
    return true;
}

uint64_t dotProduct(vector<uint64_t> x, vector<uint64_t> y)
{
    if (!areValid(x, y))
    {
        return INT64_MIN;
    }

    return inner_product(x.begin(), x.end(), y.begin(), 0);
}

vector<double> toProbabilities(vector<uint64_t> x)
{
    vector<double> distribution;
    uint64_t total = accumulate(x.begin(), x.end(), 0);

    for (size_t i = 0; i < x.size(); i++)
    {
        distribution.push_back((double) x[i] / (double) total);
    }
    return distribution;
}

vector<double> normalize(vector<uint64_t> x)
{
    vector<double> x_norm;
    for (uint64_t i = 0; i < x.size(); i++)
    {
        x_norm.push_back(x[i] * 1.0);
    }

    const int n = x_norm.size();
    const double x_mean = accumulate(x_norm.begin(), x_norm.end(), 0.0) / (double) n;

    // Calculate the sum of the squared differences from the mean.
    double sum_squared_differences = 0;
    for (int i = 0; i < n; i++)
    {
        const double difference = x_norm[i] - x_mean;
        sum_squared_differences += difference * difference;
    }
    sum_squared_differences /= (n - 1);

    if (n <= 1 || sum_squared_differences == 0)
    {
        return x_norm;
    }

    for (int i = 0; i < n; i++)
    {
        x_norm[i] = (x_norm[i] - x_mean) / sqrt(sum_squared_differences);
    }

    return x_norm;
}

double pearsonCorrelation(vector<uint64_t> x, vector<uint64_t> y)
{
    if (!areValid(x, y))
    {
        return INT64_MIN;
    }

    vector<double> x_norm = normalize(x);
    vector<double> y_norm = normalize(y);

    const int n = x.size();

    // Calculate the mean of the two vectors
    const double x_mean = accumulate(x_norm.begin(), x_norm.end(), 0.0) / (double) n;
    const double y_mean = accumulate(y_norm.begin(), y_norm.end(), 0.0) / (double) n;

    // Calculate the covariance and the standard deviations of the two vectors.
    double cov = 0;
    double x_std = 0;
    double y_std = 0;
    for (int i = 0; i < n; i++)
    {
        cov += (x_norm[i] - x_mean) * (y_norm[i] - y_mean);
        x_std += (x_norm[i] - x_mean) * (x_norm[i] - x_mean);
        y_std += (y_norm[i] - y_mean) * (y_norm[i] - y_mean);
    }

    // Check for divide-by-zero.
    if (x_std == 0 || y_std == 0)
    {
        return 0;
    }

    // Calculate the Pearson correlation coefficient.
    return cov / sqrt(x_std * y_std);
}

double klDivergence(vector<uint64_t> x, vector<uint64_t> y)
{
    if (!areValid(x, y))
    {
        return INT64_MIN;
    }

    double divergence = 0.0;

    vector<double> xProba = toProbabilities(x);
    vector<double> yProba = toProbabilities(y);

    for (size_t i = 0; i < x.size(); ++i)
    {
        divergence += xProba[i] * log((xProba[i] + 0.001) / (yProba[i] + 0.001));
    }

    return divergence;
}

double lNDistance(vector<uint64_t> x, vector<uint64_t> y, uint8_t n)
{
    if (!areValid(x, y))
    {
        return INT64_MIN;
    }

    uint64_t distance = 0;

    for (size_t i = 0; i < x.size(); i++)
    {
        uint64_t diff = x[i] > y[i] ? x[i] - y[i] : y[i] - x[i];

        distance += pow(diff, n);
    }

    return pow(distance, 1.0 / n);
}

double cosineSimilarity(vector<uint64_t> x, vector<uint64_t> y)
{
    if (!areValid(x, y))
    {
        return INT64_MIN;
    }

    uint64_t dot = dotProduct(x, y);
    double normX = sqrt(dotProduct(x, x));
    double normY = sqrt(dotProduct(y, y));

    return 1.0 - double(dot) / (normX * normY);
}

/**
 * @brief The main function here is just used for testing purposes, and is never called by DecentralizePy
 *
 */
int main()
{

    vector<uint64_t> x;
    vector<uint64_t> y;

    // x.resize(5, 1);
    // y.resize(5, -1);
    for (uint64_t i = 0; i <= 10; i++)
    {
        x.push_back(2 * i);
        y.push_back(i);
    }

    cout << "Dot product: " << dotProduct(x, y) << endl;
    cout << "Manhattan distance: " << lNDistance(x, y, 1) << endl;
    cout << "Euclidean distance: " << lNDistance(x, y, 2) << endl;
    cout << "Cosine similarity: " << cosineSimilarity(x, y) << endl;
    cout << "Pearson Correlation: " << pearsonCorrelation(x, y) << endl;
    cout << "Pearson Correlation: " << pearsonCorrelation(x, x) << endl;

    vector<double> x_norm = normalize(x);
    for (uint64_t i = 0; i <= x.size(); i++)
    {
        cout << x_norm[i] << " ";
    }
    cout << endl;

    return 0;
}