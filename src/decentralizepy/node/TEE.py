import ctypes as ct
import logging
import random as rd
from collections import deque

from decentralizepy import utils
from decentralizepy.graphs.Graph import Graph
from decentralizepy.mappings.Mapping import Mapping
from decentralizepy.node.PeerSampler import PeerSampler


class DATA(ct.Structure):
    _fields_ = [
        ("label", ct.c_size_t),
        ("count", ct.c_uint64),
    ]


class NODE(ct.Structure):
    _fields_ = [
        ("nodeId", ct.c_size_t),
        ("numData", ct.c_size_t),
        ("labels", ct.POINTER(DATA)),
    ]


class ENCLAVE(ct.Structure):
    _fields_ = [
        ("numNodes", ct.c_size_t),
        ("labels", ct.POINTER(NODE)),
    ]


class TEE(PeerSampler):
    """
    This class defines the Trusted Execution Environment service

    """

    def print_enclave(self):
        if self.enclave is not None:
            print("Label distribution:")
            tee_print = self.lib.print_tee
            tee_print.argtypes = (ct.POINTER(ENCLAVE),)
            tee_print.restype = None

            tee_print(ct.byref(self.enclave))
        else:
            logging.error("Labels have not been received properly")

    def update_sampling_rate(self, num_neighbors):
        logging.info("Updating sampling rate:")
        logging.info(
            f"Current global average neighbors per node: {self.total_neighbors / len(self.neighbors_table)}"
        )
        logging.info(f"Old sampling rate: {self.sampling_rate}")

        if num_neighbors > self.top_neighbors:
            if not self.is_rate_decreasing:
                self.is_rate_decreasing = True
                self.rate_change = max(0.001, self.rate_change / 2)

            self.sampling_rate = max(0, self.sampling_rate - self.rate_change)
        else:
            if self.is_rate_decreasing:
                self.is_rate_decreasing = False
                self.rate_change = max(0.001, self.rate_change / 2)

            self.sampling_rate = min(1, self.sampling_rate + self.rate_change)

        logging.info(f"New sampling rate: {self.sampling_rate}")

    def log_neighbors(self, seed=0):
        if seed in self.neighbors_table and len(self.neighbors_table[seed]) != 0:
            total_neighbors = 0
            for node in self.similarities.keys():
                logging.info(f"Node {node}: {self.neighbors_table[seed][node]}")
                total_neighbors += len(self.neighbors_table[seed][node])

            self.total_neighbors += total_neighbors / len(self.similarities.keys())
            logging.info(
                f"Neighbors per node: {total_neighbors / len(self.similarities.keys())}"
            )
            self.update_sampling_rate(total_neighbors / len(self.similarities.keys()))
        else:
            logging.error("Neigbors have not been computed properly")

    def get_neighbors(self, node, iteration=None):
        # If the param has not been specified, return all neighbors
        if (
            self.top_neighbors <= 0
            or self.top_neighbors >= len(self.similarities)
            or self.similarities is None
            or self.neighbors_table is None
        ):
            return self.graph.neighbors(node)

        seed = 0  # iteration

        if seed not in self.neighbors_table:
            # self.compute_neighbors_table(seed=seed)
            self.build_kreg_table(self.top_neighbors, seed=seed)

        return self.neighbors_table[seed][node]

    def receive_labels(self):
        nodes = {}
        all_labels = set()

        while set(self.my_neighbors) != set(nodes.keys()):
            sender, data = self.receive_channel("LABEL_DIST")
            if "LABELS" in data:
                logging.info(f"Labels received from {sender}")

                nodes[sender] = data["LABELS"]

                all_labels = all_labels.union(set(data["LABELS"].keys()))
            else:
                logging.error("Data received is corrupted")

        all_labels = sorted(list(all_labels))

        for node in nodes:
            label_counts = [
                DATA(label, nodes[node][label])
                if label in nodes[node]
                else DATA(label, 0)
                for label in all_labels
            ]
            nodes[node] = NODE(
                node, len(label_counts), (DATA * len(label_counts))(*label_counts)
            )

        self.enclave = ENCLAVE(len(nodes), (NODE * len(nodes))(*(list(nodes.values()))))

        logging.info("Gathered all labels from nodes")

    def compute_similarities(self):
        logging.info("Computing similarities for each node")
        similarity = self.lib.similarity
        similarity.argtypes = (ct.POINTER(ENCLAVE), ct.c_size_t, ct.c_size_t)
        similarity.restype = ct.c_double

        self.similarities = {}
        for i in self.graph.get_all_nodes():
            local_distances = {}
            for j in self.graph.get_all_nodes():
                if i != j:
                    local_distances[j] = similarity(ct.byref(self.enclave), i, j)
            self.similarities[i] = sorted(
                local_distances.items(), key=lambda kv: (kv[1], kv[0]), reverse=False
            )
            logging.info(f"Node {i}: {self.similarities[i]}")
            
    def is_connected(self, graph):
        # Start BFS at node 0
        queue = deque([0])
        
        visited = set()
        
        while queue:
            node = queue.popleft()

            if node not in visited:
                print(f'Adding node {node}')
                visited.add(node)
                queue.extend(list(graph[node]))
        print(len(visited) == len(graph))
        return len(visited) == len(graph)
    
    def build_kreg_table(self, k=4, seed=0):
        logging.info(f"Building a {k}-regular graph")

        if seed not in self.neighbors_table:
            if self.top_neighbors <= 0 or self.top_neighbors >= len(self.similarities):
                self.neighbors_table[seed] = {
                    node: self.graph.neighbors(node)
                    for node in self.similarities.keys()
                }
                return

            self.neighbors_table[seed] = {
                node: set() for node in self.similarities.keys()
            }
            is_incomplete = True
            rd.seed(seed)
            timeout = k

            while is_incomplete and timeout > 0:
                logging.info(f"Try {k + 1 - timeout}:")
                timeout -= 1
                is_incomplete = False

                for node in self.similarities.keys():
                    if len(self.neighbors_table[seed][node]) < k:
                        for peer in self.similarities[node]:
                            if (
                                peer[0] not in self.neighbors_table[seed][node]
                                and len(self.neighbors_table[seed][peer[0]]) < k
                                # and rd.random() < 0.5:
                            ):
                                self.neighbors_table[seed][node].add(peer[0])
                                self.neighbors_table[seed][peer[0]].add(node)
                                break

                for node in self.similarities.keys():
                    if len(self.neighbors_table[seed][node]) < k:
                        is_incomplete = True
                        break

            if is_incomplete:
                logging.error(f"A {k}-regular graph cannot be build with seed {seed}")
            
            if not self.is_connected(self.neighbors_table[seed]):
                logging.error(f"The resulting graph is not connected as a single partition for seed {seed}")

            logging.info(f"New neighbors table for iteration {seed}:")
            self.log_neighbors(seed=seed)

    def compute_neighbors_table(self, seed=0):
        logging.info("Computing neighbors for each node")

        if seed not in self.neighbors_table:
            if self.top_neighbors <= 0 or self.top_neighbors >= len(self.similarities):
                self.neighbors_table[seed] = {
                    node: self.graph.neighbors(node)
                    for node in self.similarities.keys()
                }
                return

            rd.seed(seed)

            # First compute outgoing edges
            self.neighbors_table[seed] = {
                node: {
                    i[0]
                    for i in self.similarities[node]
                    if i[1] < 0.0001 and rd.random() < self.sampling_rate
                }.union({(node + 1) % len(self.similarities)})
                for node in self.similarities.keys()
            }

            # Then compute incoming edges: if node i is outgoing neighbor of node j, then i is incoming neighbor of j
            for node in self.similarities.keys():
                self.neighbors_table[seed][node] = self.neighbors_table[seed][
                    node
                ].union(
                    {
                        n
                        for n in self.similarities.keys()
                        if node in self.neighbors_table[seed][n]
                    }
                )

            logging.info(f"New neighbors table for iteration {seed}:")
            self.log_neighbors(seed=seed)

    def __init__(
        self,
        rank: int,
        machine_id: int,
        mapping: Mapping,
        graph: Graph,
        config,
        tee_lib_path,
        top_neighbors=-1,
        iterations=1,
        log_dir=".",
        log_level=logging.INFO,
        *args,
    ):
        """
        Constructor

        Parameters
        ----------
        rank : int
            Rank of process local to the machine
        machine_id : int
            Machine ID on which the process in running
        mapping : decentralizepy.mappings
            The object containing the mapping rank <--> uid
        graph : decentralizepy.graphs
            The object containing the global graph
        config : dict
            A dictionary of configurations. Must contain the following:
            [DATASET]
                dataset_package
                dataset_class
                model_class
            [OPTIMIZER_PARAMS]
                optimizer_package
                optimizer_class
            [TRAIN_PARAMS]
                training_package = decentralizepy.training.Training
                training_class = Training
                epochs_per_round = 25
                batch_size = 64
        iterations : int
            Number of iterations (communication steps) for which the model should be trained
        log_dir : str
            Logging directory
        log_level : logging.Level
            One of DEBUG, INFO, WARNING, ERROR, CRITICAL
        args : optional
            Other arguments

        """
        self.instantiate(
            rank,
            machine_id,
            mapping,
            graph,
            config,
            iterations,
            log_dir,
            log_level,
            *args,
        )

        self.top_neighbors = top_neighbors
        self.sampling_rate = 0.5
        self.is_rate_decreasing = True
        self.rate_change = 0.1

        # Initialise the C/C++ library
        self.lib = ct.cdll.LoadLibrary(tee_lib_path)

        self.enclave = None
        self.similarities = None
        self.neighbors_table = {}
        self.total_neighbors = 0

        self.receive_labels()
        self.compute_similarities()

        self.print_enclave()

        self.run()

        if len(self.neighbors_table) != 0:
            logging.info(
                f"Average number of neighbors per node per iteration: {self.total_neighbors / len(self.neighbors_table)} nodes"
            )

        logging.info("TEE exiting")
