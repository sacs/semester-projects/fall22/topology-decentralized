#!/bin/bash

# Compile the c++ library
cpp_filename=tee
cpp_directory=/home/abboud/decentralizepy/src/cpp

sh $cpp_directory/compile_lib.sh $cpp_directory $cpp_filename
tee_lib_path=$cpp_directory/lib$cpp_filename.so
# End of c++ compilation

decpy_path=/home/abboud/decentralizepy/eval
cd $decpy_path

env_python=/home/abboud/decentralizepy/decpy/bin/python3
graph=/home/abboud/decentralizepy/tutorial/96_regular.edges
original_config=/home/abboud/decentralizepy/tutorial/config_cifar_sharing.ini
config_file=/home/abboud/tmp/config.ini
procs_per_machine=32
machines=3
iterations=2000
test_after=20
top_neighbors=4
eval_file=testingTEE.py
log_level=INFO

m=`cat $(grep addresses_filepath $original_config | awk '{print $3}') | grep $(/sbin/ifconfig enp3s0f0 | grep 'inet ' | awk '{print $2}') | cut -d'"' -f2`
echo M is $m
log_dir=$(date '+%Y-%m-%dT%H:%M')/machine$m
mkdir -p $log_dir

# Run the model
cp $original_config $config_file
# echo "alpha = 0.10" >> $config_file
# Removed the following arguments from command below: -ctr 0 -cte 0
$env_python $eval_file -ro 0 -tea $test_after -ld $log_dir -mid $m -ps $procs_per_machine -ms $machines -is $iterations -gf $graph -ta $test_after -cf $config_file -ll $log_level -wsd $log_dir -tee $tee_lib_path -tn $top_neighbors